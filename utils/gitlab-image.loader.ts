export default function gitlabLoader(src:string) {
  return `https://gitlab.com/Mitune_e/yufimia-embeds/-/raw/master${src}`
}