/**
 * @type {import('next').NextConfig}
 */
const nextConfig = {
  reactStrictMode: true,
  images: {
    loader: 'custom',
    loaderFile: 'utils/gitlab-image-loader.ts',
  },
}
 
module.exports = nextConfig