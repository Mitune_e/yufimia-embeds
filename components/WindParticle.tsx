// summer dream title component

import React, { useState } from 'react';

export const WindParticle: React.FC<WindParticleProps> = (props) => {

    const {width, color, delay, iterationDelay, duration } = props;

    const thisWidth = width || '256px';
    const thisColor = color || '#5493CC';
    const thisDelay = delay || 0;
    const thisIterationDelay = iterationDelay || 100;
    const thisDuration = duration || 1;

    const animation = {
        animationDuration: `${thisDuration * (100 / thisIterationDelay)}s`,
        animationIterationCount: 'infinite',
        animationDirection: 'forward',
        animationTimingFunction: 'ease-in-out',
        animationDelay: `${thisDelay}s`,
    }
    const boxStartState = {
        transform: 'translateX(-100%)',
    }
    const elemetStartState = {
        transform: 'translateX(100%)',
    }

    return (
        <div
            className="overflow-hidden"
            style={{
                ...animation,
                ...boxStartState,
                width: thisWidth,
                animationName: `windBox${thisIterationDelay}`
            }}
        >
            <svg
                style={{
                    ... animation,
                    ...elemetStartState,
                    animationName: `windElement${thisIterationDelay}`
                }}
                width="100%"
                height="19"
                viewBox="0 0 228 19"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path style={{ stroke: thisColor }} d="M1 17C12.8311 11.7605 38.9131 0.87501 75.6163 2.09456C107.735 3.16179 150.636 13.8834 177.121 13.8834C198.31 13.8834 219.202 6.02414 227 2.09451" strokeWidth="3" />
            </svg>
        </div>
    );

}

export interface WindParticleProps {
    color?: string;
    delay?: number;
    iterationDelay?: 10 | 20 | 30 | 40 | 50 | 60 | 70 | 80 | 90 | 100;
    duration?: number;
    width?: string;
}