import React from 'react';
import Link from 'next/link';

export const IndexPage: React.FC<IndexPageProps> = () => {
  return (
    <div className="flex flex-col justify-center items-center gap-3" style={{height: '100%', width: '100%'}}>
        <header className='text-xl'>Available banners</header>
        <ul>
            <li className="px-3 py-1 bg-white rounded shadow-sm text-blue-500 border">
                <Link href="/summer-dream">Summer Dream</Link>
            </li>
        </ul>
    </div>
  );
};

export interface IndexPageProps {}

export default IndexPage;