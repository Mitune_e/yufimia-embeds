import Image from 'next/image';
import { useEffect, useState } from 'react';
import React from 'react';
import Head from 'next/head';
import { SummerDreamTitle } from '../components/SummerDreamTitle';
import Link from 'next/link';
import { WindParticle } from '../components/WindParticle';
import gitlabLoader from '../utils/gitlab-image.loader';

export const SunnerDreamPage: React.FC<SummerDreamBannerProps> = () => {
  let currentValue = 0;
  const [coefX, setCoefX] = useState(0);
  const [coefY, setCoefY] = useState(0);

  let setCoefTimeout: NodeJS.Timeout;

  const CTAs = [
    { title: 'Booth', url: 'https://tsuyuno-yume.booth.pm/items/5012267', target: '_blank', color: '#5493CC' },
    { title: 'Bandcamp', url: 'https://yufimia.bandcamp.com/track/summer-dream', target: '_blank', color: '#5493CC' },
    { title: 'Spotify', url: 'https://open.spotify.com/artist/0gt3u4f3tDNC0et0l3oyGb', target: '_blank', color: '#5493CC' },
    { title: 'iTunes', url: 'https://music.apple.com/us/album/summer-dream-single/1703402555', target: '_blank', color: '#5493CC' },
  ]

  const backgroundStyle = {
    backgroundRepeat: 'no-repeat',
    backgroundAttachment: 'fixed',
    backgroundSize: '150% 150%',
  }
  const [backgroundPosition, setBackgroundPosition] = useState('50% 50%');

  const onMouseMove = throttle<React.MouseEvent<HTMLDivElement, MouseEvent>>(
    (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
      const { clientX, clientY } = e;
      const { innerWidth, innerHeight } = window;
      // we want to scale posX and posY like number between -1 and 1
      // -1 = far left/top +1 = far right/bottom
      // clientX / innerWidth gets ratio between 0 and 1
      // * 2 gets number between 0 and 2
      // -1 gets number between -1 and 1
      const x = (clientX / innerWidth) * 2 - 1;
      const y = (clientY / innerHeight) * 2 - 1;
      setCoefX(x);
      setCoefY(y);
    },
    1000 / 30
  );

  /**
   *
   * @param amplitude amplitude of movement in px
   * @param coefX % of movement that should be applied
   * @param coefY % of movement that should be applied
   */
  const parallaxTransformStyle = (
    amplitude: number,
    coefX: number,
    coefY: number,
    scale = 1
  ) => {
    const transformX = amplitude * coefX;
    const transformY = amplitude * coefY;
    return {
      transform: `scale(${scale}) translate(${transformX}px, ${transformY}px)`,
    };
  };

  let previousGamma: number;
  let previousBeta: number;

  const handleOrientation = throttle<DeviceOrientationEvent>((e) => {
    const { gamma, beta } = e as DeviceOrientationEvent;
    if (!gamma || !beta) return;

    // since gamma angle varies between 90 and -90 continuously (90 and -89 is actually the same orientation at almost 1 deg)
    // multiply this by 2 (will vary between 180 and -180)
    // sin 180 and sin -180 have same value so it wont jitter
    const measuredGammaDeg = Math.round(gamma) * 2;
    const measuredBetaDeg = Math.round(beta);

    const measuredGammaRad = (measuredGammaDeg * 2 * Math.PI) / 360;
    const measuredBetaRad = (measuredBetaDeg * 2 * Math.PI) / 360;

    if (previousGamma == undefined) previousGamma = Math.sin(measuredGammaRad);
    if (previousBeta == undefined) previousBeta = Math.sin(measuredBetaRad);

    const newSinGamma = Math.sin(measuredGammaRad);
    const newSinBeta = Math.sin(measuredBetaRad);

    const dSinGamma = newSinGamma - previousGamma;
    const dSinBeta = newSinBeta - previousBeta;

    setCoefX(previousGamma + dSinGamma);
    setCoefY(previousBeta + dSinBeta);

    previousGamma = Math.sin(measuredGammaRad);
    previousBeta = Math.sin(measuredBetaRad);
  }, 1000 / 30);
  useEffect(() => {
    window.addEventListener('deviceorientation', handleOrientation, true);

    return () => {
      window.removeEventListener('deviceorientation', handleOrientation);
    };
  }, []);

  return (
    <div
      onMouseMove={onMouseMove}
      className="bg-white flex-grow relative flex flex-col items-stretch overflow-hidden select-none relative"
    >
      <Head>
        <title>Yufimia Banner - Summer Dream</title>
      </Head>
      <div id="bannerContainer" className="flex-grow flex flex-col items-center relative">
        <div className="hidden md:block absolute top-0 left-0 right-0 bottom-0">
          <div className="absolute top-12 right-24" style={{ color:'#7E87B5'}}>New Original song</div>
          <div className="absolute bottom-16 left-24" style={{ color: '#7E87B5' }}>New Original song</div>
        </div>
        <div className="hidden md:block absolute top-0 left-0 right-0 bottom-0">
          <div className="absolute top-10 left-10">
            <img src={gitlabLoader('/public/banners/summer-dream/top left.svg')} alt="" />
          </div>
          <div className="absolute top-10 right-10">
            <img src={gitlabLoader('/public/banners/summer-dream/top right.svg')} alt="" />
          </div>
          <div className="absolute bottom-10 left-10">
            <img src={gitlabLoader('/public/banners/summer-dream/bottom left.svg')} alt="" />
          </div>
          <div className="absolute bottom-10 right-10">
            <img src={gitlabLoader('/public/banners/summer-dream/bottom right.svg')} alt="" />
          </div>
        </div>
        <div className="px-6 -mt-6 -mb-12 sm:mt-0 md:hidden">
          <SummerDreamTitle color="#5493CC" />
        </div>
        {/* a container for blue buttons */}
        <div className="
          flex gap-2 items-center justify-center
          md:absolute md:bottom-12 md:right-12 md:mt-12 md:mr-12 z-30
        ">
          {CTAs.map((item, key) => (
            <Link key={key} href={item.url} target={item.target}>
              <a
                key={key}
                style={{ backgroundColor: item.color }}
                className={`vibrate delay-1 bg-blue-500 rounded-lg py-2 px-4 text-white font-bold`}
                target={item.target}
              >
                {item.title}
              </a>
            </Link>
          ))}
        </div>
        {/* container for character and BG */}
        <div id="characterContainer" className='w-full flex-grow flex flex-col relative'>
          {/* blue wave mobile */}
          <div
            className="
              absolute top-0 left-0 right-0 bottom-0 z-10 anim-bg-x
            "
            style={{
              backgroundImage: 'url("/banners/summer-dream/wave blue.svg")',
              ...backgroundStyle,
              backgroundPosition: backgroundPosition,
            }}
          ></div>
          <div
            id="bgContainer"
            className="
              absolute top-6 left-16 right-16 bottom-8 rounded-xl	overflow-hidden
              md:top-20 md:left-24 md:right-24 md:bottom-24
            "
          >
            <div className="hidden md:block absolute right-5 z-50 w-1/2 -mt-16 lg:-mt-14 xl:-mt-8" style={{top:'2vw'}}>
              <SummerDreamTitle color="white" />
            </div>
            {/* white wave mobile */}
            <div
              className="
                absolute top-0 left-0 right-0 bottom-0 z-10 anim-bg-x
              "
              style={{
                backgroundImage: 'url("/banners/summer-dream/wave white.svg")',
                ...backgroundStyle,
                backgroundPosition: backgroundPosition
              }}
            ></div>
            <img
              src={gitlabLoader('/public/banners/summer-dream/Summer Dream-BG.png')}
              alt=""
              className="transform transition-transform duration-1000 ease-out"
              style={{
                ...parallaxTransformStyle(15, coefX, coefY, 1.2),
                pointerEvents: 'none',
              }}
            />
          </div>
          <div
            id="playButtonContainer"
            style={{width:'96px'}}
            onClick={() => {
              window.open('https://www.youtube.com/watch?v=7wLHD3Bkh8o', '_blank');
            }}
            className='absolute overflow-visible top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition scale-100 hover:scale-110 z-50 cursor-pointer'
          >
            <img
              src={gitlabLoader('/public/banners/summer-dream/play button.png')}
              alt=""
              style={{
                ...parallaxTransformStyle(-10, coefX, coefY, 0.9),
              }}
            />
          </div>
          <div
            id="characterContainer"
            className="
              absolute -left-16 -top-4 sm:top-0 bottom-0 right-0 z-20 -mb-16
              md:w-1/2 md:left-12 md:top-4 
            "
          >
            <div id="windParticlesContainer" className="absolute top-0 left-0 right-0 bottom-0 z-20">
              <div style={{ position: 'absolute', bottom: '30%', left: '90%' }}>
                <WindParticle width={'100px'} color="white" duration={0.5} delay={0.5} iterationDelay={10} />
              </div>
              <div style={{ position: 'absolute', bottom: '50%', left: '100%' }}>
                <WindParticle width={'190px'} color="white" duration={0.85} delay={0.6} iterationDelay={20} />
              </div>
              <div style={{ position: 'absolute', bottom: '70%', left: '50%' }}>
                <WindParticle width={'125px'} color="white" duration={0.65} delay={0.7} iterationDelay={30} />
              </div>
              <div style={{ position: 'absolute', bottom: '60%', left: '70%' }}>
                <WindParticle width={'200px'} color="white" duration={0.9} delay={0.8} iterationDelay={20} />
              </div>
              <div style={{ position: 'absolute', bottom: '80%', left: '60%' }}>
                <WindParticle width={'100px'} color="white" duration={0.5} delay={0.9} iterationDelay={10} />
              </div>
            </div>
            <img
              src={gitlabLoader('/public/banners/summer-dream/character.png')}
              alt=""
              style={{
                ...parallaxTransformStyle(-2, coefX, coefY, 1),
                pointerEvents: 'none',
              }}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export interface SummerDreamBannerProps {}

function throttle<T>(callback: (e: T) => void, wait: number) {
  var timeout: NodeJS.Timeout | undefined;
  return function (e: T) {
    if (timeout) return;
    timeout = setTimeout(() => (callback(e), (timeout = undefined)), wait);
  };
}

export default SunnerDreamPage;